#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;


int main(int argc, char **argv) {

	Mat dst, src;
	src = imread("F:/opencv/car.jpg");
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";
	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	blur(src, dst, Size(11, 11), Point(-1, -1));
	imshow(outputTitle, dst);

	Mat gblur;
	GaussianBlur(src, gblur, Size(11, 11), 11, 11);
	imshow("GaussianBlur", gblur);

	waitKey(0);
	return 0;
}
