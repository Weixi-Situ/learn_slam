#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {

	Mat src,dst;
	src = imread("F:/opencv/car.jpg");
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	char inputWin[] = "input image";
	namedWindow(inputWin,CV_WINDOW_AUTOSIZE);
	imshow(inputWin,src);
	
	int height = src.rows;
	int width = src.cols;
	dst = Mat::zeros(src.size(),src.type());
	float alpha = 1.2;
	float beta = 30;
	for (int row = 0; row < height; row++) {
		for (int col = 0; col < width; col++) {
			if (src.channels() == 3) {
				float b = src.at<Vec3b>(row, col)[0];
				float g = src.at<Vec3b>(row, col)[1];
				float f = src.at<Vec3b>(row, col)[2];

				dst.at<Vec3b>(row, col)[0] = saturate_cast<uchar>(b*alpha + beta);
				dst.at<Vec3b>(row, col)[1] = saturate_cast<uchar>(g*alpha + beta);
				dst.at<Vec3b>(row, col)[2] = saturate_cast<uchar>(f*alpha + beta);
			}
		}
	}

	char outputTitle[] = "contrast and brightness changge demo";
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(outputTitle,dst);

	waitKey(0);

	return 0;
}