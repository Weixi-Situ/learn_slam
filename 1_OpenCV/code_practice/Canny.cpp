#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat dst, src, gray_src;
int t1_value = 20;
int max_value = 255;
const char* outputTitle = "output image";
void CannyDemo(int, void*);

int main(int argc, char **argv) {	
	const char* inputTitle  = "input image";
	
	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	cvtColor(src,gray_src,CV_BGR2GRAY);
	createTrackbar("Threshold Value", outputTitle, &t1_value, max_value,CannyDemo);
	CannyDemo(0, 0);

	waitKey(0);
	return 0;
}

void CannyDemo(int, void*) {
	Mat edge_output;
	blur(gray_src,gray_src,Size(3,3),Point(-1,-1));
	Canny(gray_src, edge_output, t1_value, t1_value * 2, 3, false);

	dst.create(src.size(), src.type());
	src.copyTo(dst, edge_output);
	imshow(outputTitle, dst);
}
