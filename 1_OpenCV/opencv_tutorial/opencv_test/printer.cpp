#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char** argv)
{
    Mat raw(600, 800, CV_8UC3);
    for(int i=0; i<raw.rows; i++){
        for(int j=0; j<raw.cols; j++){
            raw.ptr<uchar>(i)[j*3]=255;
            raw.ptr<uchar>(i)[j*3+1]=255;
            raw.ptr<uchar>(i)[j*3+2]=255;
        }
    }
    imshow("the white image", raw);

    line(raw, Point2f(10,10), Point2f(200,200), Scalar(255, 0,0), 5, LINE_8, 0);
    //imshow("the line is printered ", raw);

    rectangle(raw, Rect(200, 200, 200, 200), Scalar(0, 255, 0), 5, LINE_4, 0);
    //imshow("the rectange is printered", raw);

    circle(raw, Point2f(500, 500), 100, Scalar(0, 0, 255), 5, LINE_AA, 0);
    imshow("the printer finished", raw);

    waitKey(0);
    return 0;
}
